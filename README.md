This is the sample repository to get started on [Integrating FastAPI with Auth0](https://dompatmore.com/blog/integrating-your-fastapi-app-with-auth0?pk_campaign=code-share&pk_kwd=bitbucket-fastapi-auth0&pk_source=bucket). 

You will need:
- Python 3.6+
- [virtualenv](https://virtualenv.pypa.io/en/stable/)
- An account on [auth0](https://auth0.com/) to get the most out of it.

---

Getting Started


1. Clone the repo and create a virtual environment within the folder.
2. Install the required modules by entering `pip install -r requirements.txt`
3. Test the project is ready by running the command `uvicorn server:app --reload` and going to the web address `localhost:8000` in your browser. If successful you should see `Hello : World`.


---
