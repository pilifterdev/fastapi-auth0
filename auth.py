from fastapi import APIRouter, Header, HTTPException, Depends
from typing import List, Optional
from jose import jwt
from six.moves.urllib.request import urlopen
import json
import ssl
from pydantic import BaseModel


class ValidToken(BaseModel):
    credentials: dict
    scopes: List[str] = []

    def hasScope(self, checkedToken):
        if checkedToken in self.scopes:
            return True
        else:
            raise HTTPException(
                status_code=403,
                detail="Not authorised to perform this action"
            )


def validToken(authorization: str = Header(None)):
    if not authorization:
        raise HTTPException(
            status_code=403,
            detail="Access Denied"
        )
    try:

        # 1. Get jwks
        jsonurl = urlopen("<YOUR-AUTH0-DOMAIN>/.well-known/jwks.json",
                          context=ssl._create_unverified_context())
        unverified_header = jwt.get_unverified_header(authorization)
        unverified_claims = jwt.get_unverified_claims(authorization)
        jwks = json.loads(jsonurl.read())
        rsa_key = {}
        for key in jwks["keys"]:
            if key['kid'] == unverified_header['kid']:
                rsa_key = {
                    "kty": key['kty'],
                    "kid": key['kid'],
                    "use": key['use'],
                    "n": key['n'],
                    "e": key['e']
                }
        #3. Decode token
        payload = jwt.decode(
            authorization, rsa_key, algorithms=['RS256'], audience='<YOUR-AUTH0-API-IDENTIFIER>', issuer="<YOUR-AUTH0-DOMAIN>")
        returnedToken = ValidToken(
            credentials=payload, scopes=payload['scope'].split(" "))
        return returnedToken

    except jwt.ExpiredSignatureError:
        raise HTTPException(
            status_code=401,
            detail="This has expired. Please fetch a new one"
        )
    except jwt.JWTError:
        raise HTTPException(
            status_code=401,
            detail="Creds are wrong"
        )
